#!/bin/bash
#################################################################
#smiaoudakis@lnw.com check service and restart if not running,
service=jenkins

if (( $(ps -ef | grep -v grep | grep $service | wc -l) > 0 ))
then
echo "$service is running!!!"
else
/etc/init.d/$service start
fi